#include <iostream>
#include <vector>
using namespace std;

void print(auto A)
{
   for (auto a : A) 
        cout <<a<<" ";

   cout<<endl;
}

void mystery1(auto& Data)
{
//SELECTION SORT
  cout<<endl<<"Mystery 1"<<endl<<"---------------------"<<endl;

  for ( int i = 0 ; i < Data.size( ) ; i++)
  {
    for ( int j = 0 ; j < i ; j++)
	if ( Data[ i ] < Data[ j ] )
	    swap( Data[ i ] , Data[ j ] );

    print(Data);
  }//end outer for (this brace is needed to include the print statement)

}

void mystery2(auto& Data)
{
//BUBBLE SORT
  cout<<endl<<"Mystery 2"<<endl<<"---------------------"<<endl;

  for (int i= 0; i < Data.size( ); i++)
  {

	for (int j= 0; j < Data.size( ) -1;j++)
	{
		if (Data[j+1] < Data[j])
		{
			swap( Data[ j ] , Data[j+1]);	
		}

	}
	print(Data);
  }
}

void mystery3(auto& Data)
{
//INSERTION SORT
  cout<<endl<<"Mystery 3"<<endl<<"---------------------"<<endl;


int key;
  for (int i= 1; i < Data.size( ); i++)
  {
	key = Data[i];
	int j= i - 1;
	while (j >= 0 && Data[j] > key)
	{
	Data[j+1] = Data[j];
	j = j -1;
	}
   Data[j+1] = key;
   print(Data);
  }
}

//... Other mysteries...

int main()
{
    
  vector<int> Data = {36, 18, 22, 30, 29, 25, 12};

  vector<int> D1 = Data;
  vector<int> D2 = Data;
  vector<int> D3 = Data;

  mystery1(D1);
  mystery2(D2);
  mystery3(D3);

}
